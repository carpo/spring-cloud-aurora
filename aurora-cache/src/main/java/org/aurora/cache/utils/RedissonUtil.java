/**
* Copyright © 2018-2019, by 晓叹星沉.
*/
package org.aurora.cache.utils;

import java.util.concurrent.TimeUnit;

import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 
 * <p>
 * redisson工具类，用于分布式锁操作，后期扩展
 * <p>使用方式：@Autowired注入即可<p>
 * <p>@Autowired <br/> RedissonUtil redissonUtil;</p>
 * @author 晓叹星沉
 * @since jdk1.8
 * 2019年6月19日
 *  
 */

public class RedissonUtil {
	
	private static final Logger logger = LoggerFactory.getLogger(RedissonUtil.class);
	
	RedissonClient redisson;
	
	/**  
	 * RedissonUtil
	 * @param redisson    
	 */
	public RedissonUtil(RedissonClient redisson) {
		super();
		this.redisson = redisson;
	}

	/**
	 * 
	 * 添加分布式锁
	 * <p>key存活时间30s，线程未结束时，会自动续航</p>
	 * <p>系统崩溃时到期会自动删除key</p>
	 * <p>使用方法</p>
	 * <pre>{@code
     * RedissonUtil redissonUtil = ...;
     * if (redissonUtil.lock(key)) {
     *   try {
     *     // do something
     *   } finally {
     *     redissonUtil.unLock(key);
     *   }
     * } else {
     *   // do other
     * }}</pre>
	 * @param key 锁的key
	 * @return
	 */
	public boolean lock(String key){
		RLock lock = redisson.getLock(key);
		return lock.tryLock();
	}
	
	/**
	 * 
	 * 添加分布式锁
	 * @see org.aurora.cache.utils.RedissonUtil#lock(java.lang.String key) 
	 * @param key 锁的key
	 * @param waitTime 等待时间，单位：秒
	 * @return
	 */
	public boolean lock(String key, int waitTime){
		RLock lock = redisson.getLock(key);
		boolean ret = false;
		try {
			ret = lock.tryLock(waitTime, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			logger.error("", e);
			e.printStackTrace();
			ret = false;
		}
		return ret;
	}
	
	/**
	 * 
	 * 添加分布式锁
	 * @see org.aurora.cache.utils.RedissonUtil#lock(java.lang.String key) 
	 * @param key 锁的key
	 * @param waitTime waitTime 等待时间
	 * @param unit 时间单位
	 * @return
	 */
	public boolean lock(String key, int waitTime, TimeUnit unit){
		RLock lock = redisson.getLock(key);
		boolean ret = false;
		try {
			ret = lock.tryLock(waitTime, unit);
		} catch (InterruptedException e) {
			logger.error("", e);
			e.printStackTrace();
			ret = false;
		}
		return ret;
	}
	
	/**
	 * 
	 * 删除锁
	 * @see org.aurora.cache.utils.RedissonUtil#lock(java.lang.String key) 
	 * @param key
	 */
	public void unLock(String key){
		RLock lock = redisson.getLock(key);
		if(lock.isHeldByCurrentThread()){
			lock.unlock();
		}
	}
	
	public void shutdown(){
		if(null != redisson){
			redisson.shutdown();
		}
	}

}
