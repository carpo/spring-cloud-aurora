/**
* Copyright © 2018-2019, by 晓叹星沉.
*/
package org.aurora.cache.utils;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.DataType;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

/**
 * 
 * <p>
 * redis工具类
 * <p>使用方式：@Autowired注入即可<p>
 * <p>@Autowired <br/> RedisUtil redisUtil;</p>
 * @author 晓叹星沉
 * @since jdk1.8
 * 2019年6月19日
 *  
 */
@Component
public class RedisUtil {
	
	private static final Logger logger = LoggerFactory.getLogger(RedisUtil.class);
	/**
	 * 默认过期时间
	 */
	private static final int DEFAULT_EXPIRE = 30000;
	
	@Autowired
	private RedisTemplate<String, Object> redisTemplate;
	/**
	 * 清空缓存
	 * @return
	 */
	public boolean flushDB() {
		try {
			Set<String> keys = redisTemplate.keys("*");
			redisTemplate.delete(keys);
			return true;
		} catch (Exception e) {
			logger.error("", e);
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * 设置过期时间
	 * 
	 * @param key
	 * @param seconds 过期时间
	 */
	public boolean expire(String key, int seconds) {
		try {  
            if(seconds > 0){  
                redisTemplate.expire(key, seconds, TimeUnit.SECONDS);  
            }  
            return true;  
        } catch (Exception e) {  
            e.printStackTrace();  
            return false;  
        }  
	}

	/**
	 * 设置默认过期时间
	 * 
	 * @param key
	 */
	public void expire(String key) {
		expire(key, DEFAULT_EXPIRE);
	}
	
	/**
	 * 	更改key
	 * 
	 * @param  oldKey 旧key
	 * @param  newKey 新key
	 * @return 状态码
	 */
	public void rename(String oldKey, String newKey) {
		try {  
		  redisTemplate.rename(oldKey, newKey);
		} catch (Exception e) {  
            e.printStackTrace();  
        }  
	}
	
	/**
	 * 更改key,仅当新key不存在时才执行
	 * 
	 * @param  oldKey 旧key
	 * @param  newKey 新key
	 * @return 状态码
	 */
	public boolean renamenx(String oldKey, String newKey) {
		try {  
			return redisTemplate.renameIfAbsent(oldKey, newKey);
		} catch (Exception e) {  
            e.printStackTrace();  
            return false;  
        }  
	}

	/**
	 * 设置key的过期时间，以秒为单位
	 * 
	 * @param  key
	 * @param seconds 过期时间 单位为秒
	 * @return 是否执行成功
	 */
	public boolean expired(String key, int seconds) {
		try {  
			return redisTemplate.expire(key, seconds, TimeUnit.SECONDS); 
		} catch (Exception e) {  
            e.printStackTrace();  
            return false;  
        }  
	}

	/**
	 * 指定key在指定的日期过期。
	 * 
	 * @param  key
	 * @param  date 过期的时间点
	 * @return 是否执行成功
	 */
	public boolean expireAt(String key, Date date) {
		try {  
			return redisTemplate.expireAt(key, date); 
		} catch (Exception e) {  
            e.printStackTrace();  
            return false;  
        }  
	}

	/**
	 * 以秒为单位，返回给定 key的剩余生存时间
	 * 
	 * @param  key
	 * @return 以秒为单位的时间表示
	 */
	public long ttl(String key) {			
		try {  
			return redisTemplate.getExpire(key);
		} catch (Exception e) {  
            e.printStackTrace();  
            return -1;  
        }
	}

	/**
	 * 取消对key过期时间的设置
	 * 
	 * @param key
	 * @return 是否设置成功
	 */
	public boolean persist(String key) {
		try {  
			return redisTemplate.persist(key);
		} catch (Exception e) {  
            e.printStackTrace();  
            return false;
        }
	}

	/**
	 * 删除keys对应的记录,可以是多个key
	 * 
	 * @param keys 删除多个key
	 * @return 删除的记录数
	 */
	public long del(List<String> keys) {
		try {  
			return redisTemplate.delete(keys);
		} catch (Exception e) {  
            e.printStackTrace();  
            return -1;
        }
	}
	
	/**
	 * 删除制定key
	 * 
	 * @param  key
	 * @return 删除的记录数
	 */
	public boolean del(String key) {
		try {  
			return redisTemplate.delete(key);
		} catch (Exception e) {  
            e.printStackTrace();  
            return false;
        }
	}

	/**
	 * 判断key是否存在
	 * 
	 * @param  key
	 * @return boolean 是否存在
	 */
	public boolean exists(String key) {
		try {  
			return redisTemplate.hasKey(key);
		} catch (Exception e) {  
            e.printStackTrace();  
            return false;
        }		
	}

	/**
	 * 返回指定key存储的类型
	 * 
	 * @param String key
	 * @return DateType的code值为：string|list|set|zset|hash|none
	 **/
	public DataType type(String key) {
		try {  
			return redisTemplate.type(key);
		} catch (Exception e) {  
            e.printStackTrace();  
            return null;
        }
	}

	/**
	 * 查找所有匹配给定的模式的键
	 * 
	 * @param String key的表达式,*表示多个，？表示一个
	 */
	public Set<String> keys(String pattern) {
		try {  
			return redisTemplate.keys(pattern);
		} catch (Exception e) {  
            e.printStackTrace();  
            return null;
        }
	}
	
	// *******************************************String***************************
	/**
	 * 根据key获取记录
	 * 
	 * @param String key
	 * @return 值
	 */
	public Object get(String key) {
		try {  
			return redisTemplate.opsForValue().get(key);
		} catch (Exception e) {
			System.out.println(e.getMessage());
            return null;
        }
	}

	/**
	 * 添加有过期时间的记录
	 * 
	 * @param String key
	 * @param Object value
	 * @param int timeout 过期时间，以秒为单位，单位秒
	 * @return String 操作状态
	 */
	public boolean setEx(String key, Object value, int timeout) {
		try {  
			redisTemplate.opsForValue().set(key, value, timeout, TimeUnit.SECONDS);
			return true;
		} catch (Exception e) {  
            e.printStackTrace();  
            return false;
        }
	}
	
	/**
	 * 添加一条记录，仅当给定的key不存在时才插入
	 * 
	 * @param String key
	 * @param Object value
	 * @return boolean
	 */
	public boolean setnx(String key, Object value) {
		try {  
			return redisTemplate.opsForValue().setIfAbsent(key, value);
		} catch (Exception e) {  
            e.printStackTrace();  
            return false;
        }
	}

	/**
	 * 添加记录,如果记录不存在则新增，存在则覆盖
	 * 
	 * @param String key
	 * @param String value
	 * @return 状态码
	 */
	public boolean set(String key, Object value) {
		try {  
			return setRange(key, 0, value);
		} catch (Exception e) {  
            e.printStackTrace();  
            return false;
        }
	}

	/**
	 * 从指定位置开始插入数据，插入的数据会覆盖指定位置以后的数据<br/>
	 * 例:String str1="123456789";<br/>
	 * 对str1操作后setRange(key,4,0000)，str1="123400009";
	 * 
	 * @param String key
	 * @param long offset
	 * @param String value
	 * @return boolean value的长度
	 */
	public boolean setRange(String key, long offset, Object value) {
		try {  
			redisTemplate.opsForValue().set(key, value, offset);
			return true;
		} catch (Exception e) {  
            e.printStackTrace();  
            return false;
        }
	}

	/**
	 * 在指定的key中追加value
	 * 
	 * @param String key
	 * @param String value
	 * @return long 追加后value的长度
	 **/
	public long append(String key, String value) {
		try {  
			return redisTemplate.opsForValue().append(key, value);
		} catch (Exception e) {  
            e.printStackTrace();  
            return -1;
        }
	}

	/**
	 * 将key对应的value减去指定的值，只有value可以转为数字时该方法才可用
	 * 
	 * @param String key
	 * @param long delta 要减去的值
	 * @return long 减指定值后的值
	 */
	public long decrBy(String key, long delta) {
		try {
			long value = delta * -1;
			return this.incrBy(key, value);
//			return redisTemplate.opsForValue().decrement(key, delta);
		} catch (Exception e) {  
            e.printStackTrace();  
            return -1;
        }
	}

	/**
	 * <b>可以作为获取唯一id的方法</b><br/>
	 * 将key对应的value加上指定的值，只有value可以转为数字时该方法才可用
	 * 
	 * @param String key
	 * @param long delta 要增加的值
	 * @return long 相加后的值
	 */
	public Long incrBy(String key, long delta) {
		try {  
			return redisTemplate.opsForValue().increment(key, delta);
		} catch (Exception e) {  
            e.printStackTrace();  
            return null;
        }
	}

	/**
	 * 对指定key对应的value进行截取
	 * 
	 * @param String key
	 * @param long startOffset 开始位置(包含)
	 * @param long endOffset 结束位置(包含)
	 * @return String 截取的值
	 */
	public String getrange(String key, long startOffset, long endOffset) {
		try {  
			return redisTemplate.opsForValue().get(key, startOffset, endOffset);
		} catch (Exception e) {  
            e.printStackTrace();  
            return null;
        }
	}

	/**
	 * 获取并设置指定key对应的value<br/>
	 * 如果key存在返回之前的value,否则返回null
	 * 
	 * @param String key
	 * @param String value
	 * @return String 原始value或null
	 */
	public Object getSet(String key, String value) {
		try {  
			return redisTemplate.opsForValue().getAndSet(key, value);
		} catch (Exception e) {  
            e.printStackTrace();  
            return null;
        }
	}

	/**
	 * 批量获取记录,如果指定的key不存在返回List的对应位置将是null
	 * 
	 * @param String keys
	 * @return List<String> 值得集合
	 */
	public List<Object> mget(List<String> keys) {
		try {  
			return redisTemplate.opsForValue().multiGet(keys);
		} catch (Exception e) {  
            e.printStackTrace();  
            return null;
        }
	}

	/**
	 * 批量存储记录
	 * 
	 * @param String keysvalues
	 *            例:keysvalues="key1","value1","key2","value2";
	 * @return String 状态码
	 */
	public boolean mset(Map<String, Object> map) {
		try {  
		   redisTemplate.opsForValue().multiSet(map);
		   return true;
		} catch (Exception e) {  
            e.printStackTrace();  
            return false;
        }
	}

	/**
	 * 获取key对应的值的长度
	 * 
	 * @param String key
	 * @return value值得长度
	 */
	public long strlen(String key) {
		try {  
		   return redisTemplate.opsForValue().size(key);
		} catch (Exception e) {  
            e.printStackTrace();  
            return -1;
        }
	}

	// *******************************************List*****************************
	
	/**
	 * List长度
	 * 
	 * @param String key
	 * @return 长度
	 */
	public long llen(String key) {
		try {  
			return redisTemplate.opsForList().size(key);
		} catch (Exception e) {  
            e.printStackTrace();  
            return -1;
        }
	}

	/**
	 * 覆盖操作,将覆盖List中指定位置的值
	 * 
	 * @param key
	 * @param int index在list中的位置
	 * @param Object value 值
	 * @return 状态码
	 */
	public boolean lset(String key, int index, Object value) {
		try {  
		   redisTemplate.opsForList().set(key, index, value);
		   return true;
		} catch (Exception e) {  
            e.printStackTrace();  
            return false;
        }
	}

	/**
	 * 获取List中指定位置的值
	 * 
	 * @param String key
	 * @param int index 位置
	 * @return 值
	 **/
	public Object lindex(String key, int index) {
		try {  
		   return redisTemplate.opsForList().index(key, index);
		} catch (Exception e) {  
            e.printStackTrace();  
            return null;
        }
	}

	/**
	 * 将List中的第一条记录移出List
	 * 
	 * @param String key
	 * @return 移出的记录
	 */
	public Object lpop(String key) {
		try {  
		   return redisTemplate.opsForList().leftPop(key);
		} catch (Exception e) {  
            e.printStackTrace();  
            return null;
        }
	}

	/**
	 * 将List中最后第一条记录移出List
	 * 
	 * @param byte[] key
	 * @return 移出的记录
	 */
	public Object rpop(String key) {
		try {  
		   return redisTemplate.opsForList().rightPop(key);
		} catch (Exception e) {  
            e.printStackTrace();  
            return null;
        }
	}

	/**
	 * 向List左侧插入vlaue
	 * 
	 * @param String key
	 * @param String value
	 * @return 记录总数
	 */
	public long lpush(String key, Object value) {
		try {  
		   return redisTemplate.opsForList().leftPush(key, value);
		} catch (Exception e) {  
            e.printStackTrace();  
            return -1;
        }
	}

	/**
	 * 向List右侧追加记录
	 * 
	 * @param String key
	 * @param String value
	 * @return 记录总数
	 */
	public long rpush(String key, String value) {
		try {  
			return redisTemplate.opsForList().rightPush(key, value);
		} catch (Exception e) {  
            e.printStackTrace();  
            return -1;
        }
	}

	/**
	 * 获取指定范围的记录，可以做为分页使用
	 * 
	 * @param String key
	 * @param long start
	 * @param long end
	 * @return List
	 */
	public List<Object> lrange(String key, long start, long end) {
		try {  
			 return redisTemplate.opsForList().range(key, start, end);
		} catch (Exception e) {  
	         e.printStackTrace();  
	         return null;
	    }
	}

	/**
	 * 删除List中c条记录，被删除的记录值为value
	 * 
	 * @param String key
	 * @param int count 要删除的数量，如果为负数则从List的尾部检查并删除符合的记录
	 * @param Object value 要匹配的值
	 * @return 删除后的List中的记录数
	 */
	public long lrem(String key, int count, Object value) {
		try {  
			return redisTemplate.opsForList().remove(key, count, value);
		} catch (Exception e) {  
	          e.printStackTrace();  
	          return -1;
	    }
	}

	/**
	 * 算是删除吧，只保留start与end之间的记录
	 * 
	 * @param String key
	 * @param int start 记录的开始位置(0表示第一条记录)
	 * @param int end 记录的结束位置（如果为-1则表示最后一个，-2，-3以此类推）
	 * @return 执行状态码
	 */
	public boolean ltrim(String key, int start, int end) {
		try {  
			redisTemplate.opsForList().trim(key, start, end);
			return true;
		} catch (Exception e) {  
          e.printStackTrace();  
          return false;
	    }
	}

	// *******************************************Sets无序集合*******************************************

	/**
	 * 向Set添加一条记录，如果member已存在返回0,否则返回1
	 * 
	 * @param String key
	 * @param String member
	 * @return 操作码,0或1
	 */
	public long sadd(String key, String member) {
		try {  
			return redisTemplate.opsForSet().add(key, member);
		} catch (Exception e) {  
            e.printStackTrace();  
            return -1;
        }
	}

	/**
	 * 获取给定key中元素个数
	 * 
	 * @param String key
	 * @return 元素个数，-1表示出现异常
	 */
	public long scard(String key) {
		try {  
			return redisTemplate.opsForSet().size(key);
		} catch (Exception e) {  
            e.printStackTrace();  
            return -1;
        }
	}

	/**
	 *  返回给定key的两个set之间的差值
	 * 
	 * @param key
	 * @param otherKeys
	 * @return 差异的成员集合
	 */
	public Set<Object> sdiff(String key, Set<String> otherKeys) {
		try {  
			return redisTemplate.opsForSet().difference(key, otherKeys);
		} catch (Exception e) {  
            e.printStackTrace();  
            return null;
		}
	}

	/**
	 * 这个命令等于sdiff,但返回的不是结果集,而是将结果集存储在新的集合中，如果目标已存在，则覆盖。
	 * 
	 * @param String key 要参与比较的集合key
	 * @param String otherKey 被比较的集合key
	 * @param String destKey 比较结果存储的集合的key
	 * @return 新集合中的记录数
	 **/
	public long sdiffstore(String key, String otherKey, String destKey) {
		try {  
			return redisTemplate.opsForSet().differenceAndStore(key, otherKey, destKey);
		} catch (Exception e) {  
            e.printStackTrace();  
            return -1;
		}
	}

	/**
	 * key对应的无序集合与多个otherKey对应的无序集合求交集
	 * 
	 * @param String key
	 * @param 
	 * @return 交集成员的集合
	 **/
	public Set<Object> sinter(String key, Set<String> otherKeys) {
		try {  
			return redisTemplate.opsForSet().intersect(key, otherKeys);
		} catch (Exception e) {  
            e.printStackTrace();  
            return null;
		}
	}

	/**
	 * key无序集合与otherkey无序集合的交集存储到destKey无序集合中。
	 * 
	 * @return 新集合中的记录数
	 **/
	public long sinterstore(String key, Set<String> otherKeys, String destKey) {
		try {  
			return redisTemplate.opsForSet().intersectAndStore(key, otherKeys, destKey);
		} catch (Exception e) {  
            e.printStackTrace();  
            return -1;
		}
	}

	/**
	 * 确定一个给定的值,在key集合中是否存在
	 * 
	 * @param String key
	 * @param String member 要判断的值
	 * @return 存在返回1，不存在返回0,调用出错返回-1
	 **/
	public int sismember(String key, String member) {
		try {  
			boolean result = redisTemplate.opsForSet().isMember(key, member);
			if(result) {
				return 1;
			} else {
				return 0;
			}
		} catch (Exception e) {  
            e.printStackTrace();  
            return -1;
		}
	}

	/**
	 * 返回集合中的所有成员
	 * 
	 * @param String key
	 * @return 成员集合
	 */
	public Set<Object> smembers(String key) {
		try {  
			return redisTemplate.opsForSet().members(key);
		} catch (Exception e) {  
            e.printStackTrace();  
            return null;
		}
	}

	/**
	 * 将成员从源集合移出放入目标集合 <br/>
	 * 如果源集合不存在或不包哈指定成员，不进行任何操作，返回0<br/>
	 * 否则该成员从源集合上删除，并添加到目标集合，如果目标集合中成员已存在，则只在源集合进行删除
	 * 
	 * @param String srckey 源集合
	 * @param String value 源集合中的成员
	 * @param String destKey 目标集合
	 * @return 状态码，1成功，0失败，调用报错
	 */
	public int smove(String srckey, String value, String destKey) {
		try {  
			boolean result = redisTemplate.opsForSet().move(srckey, value, destKey);
			if(result) {
				return 1;
			}
			return 0;
		} catch (Exception e) {  
            e.printStackTrace();  
            return -1;
		}
	}

	/**
	 * 移除并返回集合中的一个随机元素
	 * 
	 * @param String key
	 * @return 被删除的成员
	 */
	public Object spop(String key) {
		try {  
			return redisTemplate.opsForSet().pop(key);
		} catch (Exception e) {  
            e.printStackTrace();  
            return null;
		}
	}

	/**
	 * 从集合中删除指定成员
	 * 
	 * @param String key
	 * @param String values 要删除的成员
	 * @return 状态码，成功返回1，成员不存在返回0
	 */
	public long srem(String key, Object... values) {
		try {  
			return redisTemplate.opsForSet().remove(key, values);
		} catch (Exception e) {  
            e.printStackTrace();  
            return -1;
		}
	}

	/**
	 * key无序集合与otherKey无序集合的并集
	 * 
	 * @param String ... keys
	 * @return 合并后的结果集合
	 * @see sunionstore
	 */
	public Set<Object> sunion(String key, Set<String> otherKeys) {
		try {  
			return redisTemplate.opsForSet().union(key, otherKeys);
		} catch (Exception e) {  
            e.printStackTrace();  
            return null;
		}
	}

	/**
	 * 合并多个集合并将合并后的结果集保存在指定的新集合中，如果新集合已经存在则覆盖
	 * 
	 * @param String newkey 新集合的key
	 * @param String ... keys 要合并的集合
	 **/
	public long sunionstore(String key, Set<String> otherKeys, String destKey) {
		try {  
			return redisTemplate.opsForSet().unionAndStore(key, otherKeys, destKey);
		} catch (Exception e) {  
            e.printStackTrace();  
            return -1;
		}
	}

	// *******************************************ZSet有序集合*******************************************

	/**
	 * 向集合中增加一条记录,如果这个值已存在，这个值对应的权重将被置为新的权重
	 * 
	 * @param String key
	 * @param double score 权重
	 * @param String member 要加入的值，
	 * @return 状态码 1成功，0已存在member的值
	 */
	public boolean zadd(String key, String value, double score) {
		try {  
			return redisTemplate.opsForZSet().add(key, value, score);
		} catch (Exception e) {  
            e.printStackTrace();  
            return false;
		}
	}

	/**
	 * 获取集合中元素的数量
	 * 
	 * @param String key
	 * @return 如果返回0则集合不存在，返回-1表示调用失败
	 */
	public long zcard(String key) {
		try {  
			return redisTemplate.opsForZSet().zCard(key);
		} catch (Exception e) {  
            e.printStackTrace();  
            return -1;
		}
	}

	/**
	 * 获取指定权重区间内集合的数量
	 * 
	 * @param String key
	 * @param double min 最小排序位置
	 * @param double max 最大排序位置
	 * 返回值：
	 */
	public long zcount(String key, double min, double max) {
		try {  
			return redisTemplate.opsForZSet().count(key, min, max);
		} catch (Exception e) {  
            e.printStackTrace();  
            return -1;
		}
	}

	/**
	 * 获得set的长度
	 * 
	 * @param key
	 * @return
	 */
	public long zlength(String key) {
		try {  
			return redisTemplate.opsForZSet().size(key);
		} catch (Exception e) {  
            e.printStackTrace();  
            return -1;
		}
	}

	/**
	 * 权重增加给定值，如果给定的value已存在
	 * 
	 * @param String key
	 * @param Object value 要插入的值
	 * @param double score 要增的权重
	 * @return 增后的权重
	 */
	public double zincrby(String key, String value, double delta) {
		
		try {  
			return redisTemplate.opsForZSet().incrementScore(key, value, delta);
		} catch (Exception e) {  
            e.printStackTrace();  
            return -1;
		}
	}

	/**
	 * 返回指定位置的集合元素,0为第一个元素，-1为最后一个元素
	 * 
	 * @param String key
	 * @param int start 开始位置(包含)
	 * @param int end 结束位置(包含)
	 * @return Set<String>
	 */
	public Set<Object> zrange(String key, int start, int end) {
		try {  
			return redisTemplate.opsForZSet().range(key, start, end);
		} catch (Exception e) {  
            e.printStackTrace();  
            return null;
		}
	}

	/**
	 * 返回指定权重区间的元素集合
	 * 
	 * @param String key
	 * @param double min 上限权重
	 * @param double max 下限权重
	 * @return Set<String>
	 */
	public Set<Object> zrangeByScore(String key, double min, double max) {
		try {  
			return redisTemplate.opsForZSet().rangeByScore(key, min, max);
		} catch (Exception e) {  
            e.printStackTrace();  
            return null;
		}
	}

	/**
	 * 返回有序集中指定成员的排名，其中有序集成员按分数值递增(从小到大)顺序排列
	 * 
	 * @see zrevrank
	 * @param String key
	 * @param String value
	 * @return long 位置
	 */
	public long zrank(String key, String value) {
		try {  
			return redisTemplate.opsForZSet().rank(key, value);
		} catch (Exception e) {  
            e.printStackTrace();  
            return -1;
		}
	}

	/**
	 * 返回有序集中指定成员的排名，其中有序集成员按分数值递减(从大到小)顺序排列
	 * 
	 * @see zrank
	 * @param String key
	 * @param String value
	 * @return long 位置或者排名
	 */
	public long zrevrank(String key, String value) {
		try {  
			return redisTemplate.opsForZSet().reverseRank(key, value);
		} catch (Exception e) {  
            e.printStackTrace();  
            return -1;
		}
	}

	/**
	 * 从集合中删除成员
	 * 
	 * @param String key
	 * @param Object values
	 * @return 返回1成功
	 */
	public long zrem(String key, Object values) {
		try {  
			return redisTemplate.opsForZSet().remove(key, values);
		} catch (Exception e) {  
            e.printStackTrace();  
            return -1;
		}
	}

	/**
	 * 删除
	 * 
	 * @param key
	 * @return
	 */
	public boolean zrem(String key) {
		try {  
			return redisTemplate.delete(key);
		} catch (Exception e) {  
            e.printStackTrace();  
            return false;
		}
	}

	/**
	 * 删除给定位置区间的元素
	 * 
	 * @param String key
	 * @param int start 开始区间，从0开始(包含)
	 * @param int end 结束区间,-1为最后一个元素(包含)
	 * @return 删除的数量
	 */
	public long zremrangeByRank(String key, int start, int end) {
		try {  
			return redisTemplate.opsForZSet().removeRange(key, start, end);
		} catch (Exception e) {  
            e.printStackTrace();  
            return -1;
		}
	}

	/**
	 * 删除给定权重区间的元素
	 * 
	 * @param String key
	 * @param double min 下限权重(包含)
	 * @param double max 上限权重(包含)
	 * @return 删除的数量
	 */
	public long zremrangeByScore(String key, double min, double max) {
		try {  
			return redisTemplate.opsForZSet().removeRangeByScore(key, min, max);
		} catch (Exception e) {  
            e.printStackTrace();  
            return -1;
		}
	}

	/**
	 * 通过索引区间返回有序集合成指定区间内的成员，其中有序集成员按分数值递减(从大到小)顺序排列
	 * 
	 * @param String key
	 * @param long start
	 * @param long end
	 * @return Set<String>
	 */
	public Set<Object> zrevrange(String key, long start, long end) {
		try {  
			return redisTemplate.opsForZSet().reverseRange(key, start, end);
		} catch (Exception e) {  
            e.printStackTrace();  
            return null;
		}
	}

	/**
	 * 获取给定值在集合中的权重
	 * 
	 * @param String key
	 * @param Object value
	 * @return double 权重
	 */
	public double zscore(String key, String value) {
		try {  
			return redisTemplate.opsForZSet().score(key, value);
		} catch (Exception e) {  
            e.printStackTrace();  
            return -1;
		}			
	}

	// *******************************************Hash*******************************************
	/**
	 * 从hash中删除指定的存储
	 * 
	 * @param String key
	 * @param String hashKeys 存储的名字
	 * @return 状态码，1成功，0失败
	 */
	public long hdel(String key, Object... hashKeys) {
		try {  
			return redisTemplate.opsForHash().delete(key, hashKeys);
		} catch (Exception e) {  
            e.printStackTrace();  
            return -1;
		}
	}

	public boolean hdel(String key) {
		try {  
			return redisTemplate.delete(key);
		} catch (Exception e) {  
            e.printStackTrace();  
            return false;
		}
	}

	/**
	 * 测试hash中指定的存储是否存在
	 * 
	 * @param String key
	 * @param String hashKey 存储的key
	 * @return 1存在，0不存在
	 */
	public boolean hexists(String key, String hashKey) {
		try {  
			return redisTemplate.opsForHash().hasKey(key, hashKey);
		} catch (Exception e) {  
            e.printStackTrace();  
            return false;
		}
	}

	/**
	 * 返回hash中指定存储位置的值
	 * 
	 * @param String key
	 * @param String fieid 存储的名字
	 * @return 存储对应的值
	 */
	public Object hget(String key, String hashKey) {
		try {  
			return redisTemplate.opsForHash().get(key, hashKey);
		} catch (Exception e) {  
            e.printStackTrace();  
            return null;
		}
	}

	/**
	 * 以Map的形式返回hash中的存储和值
	 * 
	 * @param String key
	 * @return Map<Object, Object>
	 */
	public Map<Object, Object> hgetAll(String key) {
		try {  
			return redisTemplate.opsForHash().entries(key);
		} catch (Exception e) {  
            e.printStackTrace();  
            return null;
		}
	}

	/**
	 * 添加一个对应关系
	 * 
	 * @param String key
	 * @param String hashKey
	 * @param Object value
	 * @return 
	 **/
	public boolean hset(String key, String hashKey, Object value) {
		try {  
		   redisTemplate.opsForHash().put(key, hashKey, value);
		   return true;
		} catch (Exception e) {  
            e.printStackTrace();  
            return false;
		}
	}

	/**
	 * 添加对应关系，只有在hashKey不存在时才执行
	 * 
	 * @param String key
	 * @param String hashKey
	 * @param String value
	 * @return
	 **/
	public boolean hsetnx(String key, String hashKey, String value) {
		try {  
			   return redisTemplate.opsForHash().putIfAbsent(key, hashKey, value);
		} catch (Exception e) {  
            e.printStackTrace();  
            return false;
		}
	}

	/**
	 * 获取hash中value的集合
	 * 
	 * @param String key
	 * @return List<String>
	 */
	public List<Object> hvals(String key) {
		try {  
			return redisTemplate.opsForHash().values(key);
		} catch (Exception e) {  
            e.printStackTrace();  
            return null;
		}
	}

	/**
	 * 在指定的存储位置加上指定的数字，存储位置的值必须可转为数字类型
	 * 
	 * @param String key
	 * @param String hashKey 存储key
	 * @param String long delta 要增加的值,可以是负数
	 * @return 增加指定数字后，存储位置的值
	 */
	public long hincrby(String key, String hashKey, long delta) {
		try {  
			return redisTemplate.opsForHash().increment(key, hashKey, delta);
		} catch (Exception e) {  
            e.printStackTrace();  
            return -1;
		}
	}

	/**
	 * 返回指定hash中的所有存储名字,类似Map中的keySet方法
	 * 
	 * @param String key
	 * @return Set<Object> 存储名称的集合
	 */
	public Set<Object> hkeys(String key) {
		try {  
			return redisTemplate.opsForHash().keys(key);
		} catch (Exception e) {  
            e.printStackTrace();  
            return null;
		}
	}

	/**
	 * 获取hash中存储的个数，类似Map中size方法
	 * 
	 * @param String key
	 * @return long 存储的个数
	 */
	public long hlen(String key) {
		try {  
			return redisTemplate.opsForHash().size(key);
		} catch (Exception e) {  
            e.printStackTrace();  
            return -1;
		}
	}

	/**
	 * 根据多个key，获取对应的value，返回List,如果指定的key不存在,List对应位置为null
	 * 
	 * @param String key
	 * @param List<Object> ... hashKeys 存储位置
	 * @return List<String>
	 */
	public List<Object> hmget(String key, List<Object> hashKeys) {
		try {  
			 return redisTemplate.opsForHash().multiGet(key, hashKeys);
		} catch (Exception e) {  
            e.printStackTrace();  
            return null;
		}
	}

	/**
	 * 添加对应关系，如果对应关系已存在，则覆盖
	 * 
	 * @param Strin key
	 * @param Map <String,Object> 对应关系
	 * @return
	 */
	public boolean hmset(String key, Map<String, Object> map) {
		try {  
			redisTemplate.opsForHash().putAll(key, map);
			return true;
		} catch (Exception e) {  
            e.printStackTrace();  
            return false;
		}
	}

}
