package org.aurora.cache.config;

import java.net.URI;

/**
 * @Description : redis连接属性
 * @Author : liuhl-b
 * @Date : 2023/10/31 12:28
 **/
public class ConnectionInfo {

    private final URI uri;

    private final boolean useSsl;

    private final String username;

    private final String password;

    ConnectionInfo(URI uri, boolean useSsl, String username, String password) {
        this.uri = uri;
        this.useSsl = useSsl;
        this.username = username;
        this.password = password;
    }

    boolean isUseSsl() {
        return this.useSsl;
    }

    String getHostName() {
        return this.uri.getHost();
    }

    int getPort() {
        return this.uri.getPort();
    }

    String getUsername() {
        return this.username;
    }

    String getPassword() {
        return this.password;
    }
}
