/**
* Copyright © 2018-2019, by 晓叹星沉.
*/
package org.aurora.cache.config;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.aurora.cache.utils.RedissonUtil;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.context.properties.PropertyMapper;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.*;
import org.springframework.data.redis.connection.jedis.JedisClientConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.connection.lettuce.LettuceClientConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.connection.lettuce.LettucePoolingClientConfiguration;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.util.ClassUtils;
import redis.clients.jedis.JedisPoolConfig;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * <p>
 * redis config
 * </p>
 * @author 晓叹星沉
 * @since jdk1.8
 * 2019年6月18日
 *  
 */
@Configuration
@EnableCaching 
@EnableConfigurationProperties(RedisProperties.class)
public class RedisConfig {
	
	private final int REDIS_DEFAULT_TIME_OUT = 30000;
	
	private final String REDISSON_PREFIX = "redis://";

	private static final boolean COMMONS_POOL2_AVAILABLE = ClassUtils.isPresent("org.apache.commons.pool2.ObjectPool",
			RedisConfig.class.getClassLoader());

	@Autowired
	RedisProperties properties;
	
	@Bean
	public RedisTemplate<String, Object> redisTemplate() {
		RedisTemplate<String, Object> redisTemplate = new RedisTemplate<>();
		redisTemplate.setConnectionFactory(getLocalConnectionFactory());
		// 设置键（key）的序列化采用StringRedisSerializer
        GenericJackson2JsonRedisSerializer jackson2JsonRedisSerializer = new GenericJackson2JsonRedisSerializer();
        // 设置值（value）的序列化采用FastJsonRedisSerializer
        redisTemplate.setValueSerializer(jackson2JsonRedisSerializer);
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        redisTemplate.setHashKeySerializer(new StringRedisSerializer());
        redisTemplate.afterPropertiesSet();
		return redisTemplate;
		
	}
	
	@Bean(destroyMethod="shutdown")
	@ConditionalOnMissingBean
	public RedissonUtil redissonFactory(){
		int timeOut = REDIS_DEFAULT_TIME_OUT;
		if(null != properties && null != properties.getTimeout() && properties.getTimeout().toMillis() > 0){
			timeOut = (int)properties.getTimeout().toMillis();
		}
		
		String password = null;
		if(StringUtils.isNotBlank(properties.getPassword())){
			password = properties.getPassword();
		}
		
		Config config = new Config();
		if(null != properties.getCluster()){//cluster模式尚未验证
			List<String> list = properties.getCluster().getNodes();
			String[] address = new String[list.size()];  
			int i = 0;
			for (String add : list) {
				address[i] = REDISSON_PREFIX + add;
				i++;
			}
			config.useClusterServers().setScanInterval(2000)
			.addNodeAddress(address).setPassword(password);
		}else if(null != properties.getSentinel()){
			List<String> list = properties.getSentinel().getNodes();
			password = properties.getSentinel().getPassword();
			String[] address = new String[list.size()];  
			int i = 0;
			for (String add : list) {
				address[i] = REDISSON_PREFIX + add;
			}
			config.useSentinelServers().setMasterName(properties.getSentinel().getMaster())
			.addSentinelAddress(address).setPassword(password).setTimeout(timeOut).setDatabase(properties.getDatabase());
		}else{
			config.useSingleServer().setAddress(REDISSON_PREFIX + properties.getHost() + ":" + properties.getPort())
			.setPassword(password).setTimeout(timeOut).setDatabase(properties.getDatabase());
		}
		
		RedissonClient redisson = Redisson.create(config);
		return new RedissonUtil(redisson);
	}

	/**
	 * 获取本地连接工程
	 * @return
	 */
	private RedisConnectionFactory getLocalConnectionFactory() {
		if (null != properties.getClientType() && properties.getClientType().equals(org.springframework.boot.autoconfigure.data.redis.RedisProperties.ClientType.LETTUCE)){
			return createLettuceConnectionFactory();
		}else {
			return this.createJedisConnectionFactory();
		}

	}

	private LettuceConnectionFactory createLettuceConnectionFactory() {
		LettuceClientConfiguration clientConfiguration = getLettuceClientConfiguration(getProperties().getLettuce().getPool());
		LettuceConnectionFactory lettuceConnectionFactory = null;
		if (getSentinelConfig() != null) {
			lettuceConnectionFactory = new LettuceConnectionFactory(getSentinelConfig(), clientConfiguration);
		} else if (getClusterConfiguration() != null) {
			lettuceConnectionFactory = new LettuceConnectionFactory(getClusterConfiguration(), clientConfiguration);
		}else {
			lettuceConnectionFactory = new LettuceConnectionFactory(getStandaloneConfig(), clientConfiguration);
		}
		lettuceConnectionFactory.afterPropertiesSet();
		return lettuceConnectionFactory;
	}


	private JedisConnectionFactory createJedisConnectionFactory() {
		JedisClientConfiguration clientConfiguration = getJedisClientConfiguration();
		JedisConnectionFactory jedisConnectionFactory = null;
		if (getSentinelConfig() != null) {
			jedisConnectionFactory = new JedisConnectionFactory(getSentinelConfig(), clientConfiguration);
		}else if (getClusterConfiguration() != null) {
			jedisConnectionFactory = new JedisConnectionFactory(getClusterConfiguration(), clientConfiguration);
		}else {
			jedisConnectionFactory = new JedisConnectionFactory(getStandaloneConfig(), clientConfiguration);
		}
		jedisConnectionFactory.afterPropertiesSet();
		return jedisConnectionFactory;
	}

	private LettuceClientConfiguration getLettuceClientConfiguration(
			org.springframework.boot.autoconfigure.data.redis.RedisProperties.Pool pool) {
		LettuceClientConfiguration.LettuceClientConfigurationBuilder builder = createBuilder(pool);
		applyProperties(builder);
		if (org.springframework.util.StringUtils.hasText(getProperties().getUrl())) {
			customizeConfigurationFromUrl(builder);
		}
		return builder.build();
	}

	private void customizeConfigurationFromUrl(LettuceClientConfiguration.LettuceClientConfigurationBuilder builder) {
		ConnectionInfo connectionInfo = parseUrl(getProperties().getUrl());
		if (connectionInfo.isUseSsl()) {
			builder.useSsl();
		}
	}

	private LettuceClientConfiguration.LettuceClientConfigurationBuilder applyProperties(
			LettuceClientConfiguration.LettuceClientConfigurationBuilder builder) {
		if (getProperties().isSsl()) {
			builder.useSsl();
		}
		if (getProperties().getTimeout() != null) {
			builder.commandTimeout(getProperties().getTimeout());
		}
		if (getProperties().getLettuce() != null) {
			org.springframework.boot.autoconfigure.data.redis.RedisProperties.Lettuce lettuce = getProperties().getLettuce();
			if (lettuce.getShutdownTimeout() != null && !lettuce.getShutdownTimeout().isZero()) {
				builder.shutdownTimeout(getProperties().getLettuce().getShutdownTimeout());
			}
		}
		if (org.springframework.util.StringUtils.hasText(getProperties().getClientName())) {
			builder.clientName(getProperties().getClientName());
		}
		return builder;
	}

	private LettuceClientConfiguration.LettuceClientConfigurationBuilder createBuilder(org.springframework.boot.autoconfigure.data.redis.RedisProperties.Pool pool) {
		if (isPoolEnabled(pool)) {
			return new PoolBuilderFactory().createBuilder(pool);
		}
		return LettuceClientConfiguration.builder();
	}

	/**
	 * Inner class to allow optional commons-pool2 dependency.
	 */
	private static class PoolBuilderFactory {
		LettuceClientConfiguration.LettuceClientConfigurationBuilder createBuilder(org.springframework.boot.autoconfigure.data.redis.RedisProperties.Pool properties) {
			return LettucePoolingClientConfiguration.builder().poolConfig(getPoolConfig(properties));
		}


		private GenericObjectPoolConfig<?> getPoolConfig(org.springframework.boot.autoconfigure.data.redis.RedisProperties.Pool properties) {
			GenericObjectPoolConfig<?> config = new GenericObjectPoolConfig<>();
			config.setMaxTotal(properties.getMaxActive());
			config.setMaxIdle(properties.getMaxIdle());
			config.setMinIdle(properties.getMinIdle());
			if (properties.getTimeBetweenEvictionRuns() != null) {
				config.setTimeBetweenEvictionRuns(properties.getTimeBetweenEvictionRuns());
			}
			if (properties.getMaxWait() != null) {
				config.setMaxWait(properties.getMaxWait());
			}
			return config;
		}

	}

	protected final RedisStandaloneConfiguration getStandaloneConfig() {
		RedisStandaloneConfiguration config = new RedisStandaloneConfiguration();
		if (org.springframework.util.StringUtils.hasText(this.properties.getUrl())) {
			ConnectionInfo connectionInfo = parseUrl(this.properties.getUrl());
			config.setHostName(connectionInfo.getHostName());
			config.setPort(connectionInfo.getPort());
			config.setUsername(connectionInfo.getUsername());
			config.setPassword(RedisPassword.of(connectionInfo.getPassword()));
		}
		else {
			config.setHostName(this.properties.getHost());
			config.setPort(this.properties.getPort());
			config.setUsername(this.properties.getUsername());
			config.setPassword(RedisPassword.of(this.properties.getPassword()));
		}
		config.setDatabase(this.properties.getDatabase());
		return config;
	}

	protected ConnectionInfo parseUrl(String url) {
		try {
			URI uri = new URI(url);
			String scheme = uri.getScheme();
			if (!"redis".equals(scheme) && !"rediss".equals(scheme)) {
				throw new RuntimeException(url);
			}
			boolean useSsl = ("rediss".equals(scheme));
			String username = null;
			String password = null;
			if (uri.getUserInfo() != null) {
				String candidate = uri.getUserInfo();
				int index = candidate.indexOf(':');
				if (index >= 0) {
					username = candidate.substring(0, index);
					password = candidate.substring(index + 1);
				}
				else {
					password = candidate;
				}
			}
			return new ConnectionInfo(uri, useSsl, username, password);
		}
		catch (URISyntaxException ex) {
			throw new RuntimeException(url, ex);
		}
	}

	/**
	 * Create a {@link RedisClusterConfiguration} if necessary.
	 * @return {@literal null} if no cluster settings are set.
	 */
	protected final RedisClusterConfiguration getClusterConfiguration() {
		if (this.properties.getCluster() == null) {
			return null;
		}
		org.springframework.boot.autoconfigure.data.redis.RedisProperties.Cluster clusterProperties = this.properties.getCluster();
		RedisClusterConfiguration config = new RedisClusterConfiguration(clusterProperties.getNodes());
		if (clusterProperties.getMaxRedirects() != null) {
			config.setMaxRedirects(clusterProperties.getMaxRedirects());
		}
		config.setUsername(this.properties.getUsername());
		if (this.properties.getPassword() != null) {
			config.setPassword(RedisPassword.of(this.properties.getPassword()));
		}
		return config;
	}

	protected final RedisSentinelConfiguration getSentinelConfig() {
		org.springframework.boot.autoconfigure.data.redis.RedisProperties.Sentinel sentinelProperties = this.properties.getSentinel();
		if (sentinelProperties != null) {
			RedisSentinelConfiguration config = new RedisSentinelConfiguration();
			config.master(sentinelProperties.getMaster());
			config.setSentinels(createSentinels(sentinelProperties));
			config.setUsername(this.properties.getUsername());
			if (this.properties.getPassword() != null) {
				config.setPassword(RedisPassword.of(this.properties.getPassword()));
			}
			config.setSentinelUsername(sentinelProperties.getUsername());
			if (sentinelProperties.getPassword() != null) {
				config.setSentinelPassword(RedisPassword.of(sentinelProperties.getPassword()));
			}
			config.setDatabase(this.properties.getDatabase());
			return config;
		}
		return null;
	}

	private List<RedisNode> createSentinels(org.springframework.boot.autoconfigure.data.redis.RedisProperties.Sentinel sentinel) {
		List<RedisNode> nodes = new ArrayList<>();
		for (String node : sentinel.getNodes()) {
			try {
				nodes.add(RedisNode.fromString(node));
			}
			catch (RuntimeException ex) {
				throw new IllegalStateException("Invalid redis sentinel property '" + node + "'", ex);
			}
		}
		return nodes;
	}

	private JedisClientConfiguration getJedisClientConfiguration() {
		JedisClientConfiguration.JedisClientConfigurationBuilder builder = applyProperties(JedisClientConfiguration.builder());
		org.springframework.boot.autoconfigure.data.redis.RedisProperties.Pool pool = getProperties().getJedis().getPool();
		if (isPoolEnabled(pool)) {
			applyPooling(pool, builder);
		}
		if (org.springframework.util.StringUtils.hasText(getProperties().getUrl())) {
			customizeConfigurationFromUrl(builder);
		}
		//builderCustomizers.orderedStream().forEach((customizer) -> customizer.customize(builder));
		return builder.build();
	}

	private void customizeConfigurationFromUrl(JedisClientConfiguration.JedisClientConfigurationBuilder builder) {
		ConnectionInfo connectionInfo = parseUrl(getProperties().getUrl());
		if (connectionInfo.isUseSsl()) {
			builder.useSsl();
		}
	}

	private JedisClientConfiguration.JedisClientConfigurationBuilder applyProperties(JedisClientConfiguration.JedisClientConfigurationBuilder builder) {
		PropertyMapper map = PropertyMapper.get().alwaysApplyingWhenNonNull();
		map.from(getProperties().isSsl()).whenTrue().toCall(builder::useSsl);
		map.from(getProperties().getTimeout()).to(builder::readTimeout);
		map.from(getProperties().getConnectTimeout()).to(builder::connectTimeout);
		map.from(getProperties().getClientName()).whenHasText().to(builder::clientName);
		return builder;
	}

	protected RedisProperties getProperties() {
		return this.properties;
	}

	protected boolean isPoolEnabled(org.springframework.boot.autoconfigure.data.redis.RedisProperties.Pool pool) {
		Boolean enabled = pool.getEnabled();
		return (enabled != null) ? enabled : COMMONS_POOL2_AVAILABLE;
	}

	private void applyPooling(org.springframework.boot.autoconfigure.data.redis.RedisProperties.Pool pool,
							  JedisClientConfiguration.JedisClientConfigurationBuilder builder) {
		builder.usePooling().poolConfig(jedisPoolConfig(pool));
	}

	private JedisPoolConfig jedisPoolConfig(org.springframework.boot.autoconfigure.data.redis.RedisProperties.Pool pool) {
		JedisPoolConfig config = new JedisPoolConfig();
		config.setMaxTotal(pool.getMaxActive());
		config.setMaxIdle(pool.getMaxIdle());
		config.setMinIdle(pool.getMinIdle());
		if (pool.getTimeBetweenEvictionRuns() != null) {
			config.setTimeBetweenEvictionRuns(pool.getTimeBetweenEvictionRuns());
		}
		if (pool.getMaxWait() != null) {
			config.setMaxWait(pool.getMaxWait());
		}
		return config;
	}

}
