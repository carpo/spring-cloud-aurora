package org.aurora.cache;

import org.aurora.cache.utils.RedisUtil;
import org.aurora.cache.utils.RedissonUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;


@SpringBootTest
public class AuroraApplicationTests {

	@Autowired
	private RedisUtil redis;

	@Autowired
	RedissonUtil redisson;
	
	@Test
	public void contextLoads() {
		GroupTest t = new GroupTest();
		t.setName("123");
		redis.set("211liuhl-test", t);
		//System.out.println(redis.opsForValue().get("211liuhl-test"));
		System.out.println(((GroupTest)redis.get("211liuhl-test")).getName());
		 
		//System.out.println(redisson.lock("test"));
	}
	

}
