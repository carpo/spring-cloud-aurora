/**
* Copyright © 2017-2018, by 晓叹星沉.
*/
package org.aurora.cache;

import java.io.Serializable;
import java.util.Date;


/**
 * 
 * 此处填写类简介
 * <p>
 * 此处填写类说明
 * </p>
 * @author 晓叹星沉
 * @since jdk1.8
 * 2018年6月2日
 *  
 */

public class GroupTest implements Serializable {
    private String id;
    private String name;
    private Date creatDate;
    private Date startDate;
    
    

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

	public Date getCreatDate() {
		return creatDate;
	}

	public void setCreatDate(Date creatDate) {
		this.creatDate = creatDate;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	
    
}
