/**
* Copyright © 2018-2019, by 晓叹星沉.
*/
package org.aurora.mq.core;

import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.client.producer.TransactionListener;
import org.apache.rocketmq.client.producer.TransactionMQProducer;
import org.apache.rocketmq.common.message.Message;

/**
 * 
 * <p>
 * rocketMQ transaction producer基类
 * </p>
 * @author 晓叹星沉
 * @since jdk1.8
 * 2019年7月10日
 *  
 */

public abstract class AbstractRocketMQTransactionProducer implements TransactionListener {

	private TransactionMQProducer transactionProducer;

	/**
	 * 
	 * 发送事务消息
	 * @param topic 主题
	 * @param tags tag
	 * @param key 全局唯一标识
	 * @param body 内容
	 * @param arg 附加参数
	 * @return SendResult 发送结果
	 * @throws MQClientException
	 */
	public SendResult sendMessageInTransaction(String topic, String tags, String key, byte[] body, Object arg) throws MQClientException{
		Message msg = initMsg(topic, tags, key, body);
        return transactionProducer.sendMessageInTransaction(msg, arg);
    }
	
	/**
	 * 初始化消息
	 * @param topic
	 * @param tags
	 * @param key
	 * @param body
	 * @return
	 */
	private Message initMsg(String topic, String tags, String key, byte[] body) {
		Message msg = new Message(topic, tags, key, body);
		return msg;
	}

	public void setTransactionProducer(TransactionMQProducer transactionProducer) {
		this.transactionProducer = transactionProducer;
	}
	

}
