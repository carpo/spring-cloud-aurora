/**
* Copyright © 2018-2019, by 晓叹星沉.
*/
package org.aurora.mq.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 
 * <p>
 * RocketMq配置类
 * </p>
 * @author 晓叹星沉
 * @since jdk1.8
 * 2019年7月9日
 *  
 */
@ConfigurationProperties(prefix = "spring.rocketmq")
public class RocketMQProperties {
	
	/**
	 * 生产者组
	 */
	private String producerGroup;
	
	/**
	 * broker地址
	 */
	private String nameServerAddress;
	
	/**
     * 发送超时时间
     */
    private Integer sendTimeout = 3000;

    /**
     * vip channel
     */
    private Boolean vipChannelEnabled = Boolean.TRUE;

	public String getProducerGroup() {
		return producerGroup;
	}

	public void setProducerGroup(String producerGroup) {
		this.producerGroup = producerGroup;
	}

	public String getNameServerAddress() {
		return nameServerAddress;
	}

	public void setNameServerAddress(String nameServerAddress) {
		this.nameServerAddress = nameServerAddress;
	}

	public Integer getSendTimeout() {
		return sendTimeout;
	}

	public void setSendTimeout(Integer sendTimeout) {
		this.sendTimeout = sendTimeout;
	}

	public Boolean getVipChannelEnabled() {
		return vipChannelEnabled;
	}

	public void setVipChannelEnabled(Boolean vipChannelEnabled) {
		this.vipChannelEnabled = vipChannelEnabled;
	}
    
    

}
