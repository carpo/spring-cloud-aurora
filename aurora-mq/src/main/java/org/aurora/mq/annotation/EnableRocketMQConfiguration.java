/**
* Copyright © 2018-2019, by 晓叹星沉.
*/
package org.aurora.mq.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.aurora.mq.config.RocketMQAutoConfiguration;
import org.springframework.context.annotation.Import;

/**
 * 
 * @author 晓叹星沉
 * @since jdk1.8
 * 2019年7月9日
 *  
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@Import(RocketMQAutoConfiguration.class)
public @interface EnableRocketMQConfiguration {

}
