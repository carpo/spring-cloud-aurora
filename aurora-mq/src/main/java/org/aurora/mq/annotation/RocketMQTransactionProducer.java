/**
* Copyright © 2018-2019, by 晓叹星沉.
*/
package org.aurora.mq.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.stereotype.Component;

/**
 * 
 * <p>
 * 事务生产者注解
 * </p>
 * @author 晓叹星沉
 * @since jdk1.8
 * 2019年7月9日
 *  
 */

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
@Inherited
public @interface RocketMQTransactionProducer {

	/**
	 * 
	 * 生产者组，事务回调查询同组只调一个
	 */
	String producerGroup(); 
}
