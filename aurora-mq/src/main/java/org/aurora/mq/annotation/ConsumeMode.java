/**
* Copyright © 2018-2019, by 晓叹星沉.
*/
package org.aurora.mq.annotation;

/**
 * 
 * <p>
 * 消费模式
 * </p>
 * @author 晓叹星沉
 * @since jdk1.8
 * 2019年7月9日
 *  
 */

public enum ConsumeMode {

	/**
     * Receive asynchronously delivered messages concurrently
     */
    CONCURRENTLY,

    /**
     * Receive asynchronously delivered messages orderly. one queue, one thread
     */
    ORDERLY
}
