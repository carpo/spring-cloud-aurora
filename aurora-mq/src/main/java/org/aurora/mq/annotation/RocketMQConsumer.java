/**
* Copyright © 2018-2019, by 晓叹星沉.
*/
package org.aurora.mq.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.apache.rocketmq.common.consumer.ConsumeFromWhere;
import org.apache.rocketmq.common.protocol.heartbeat.MessageModel;
import org.springframework.stereotype.Component;


/**
 * 
 * <p>
 * rocketMQ消费者注解
 * </p>
 * @author 晓叹星沉
 * @since jdk1.8
 * 2019年7月9日
 *  
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
public @interface RocketMQConsumer {

	/**
	 * 
	 * 消费者组
	 * 相同组：同一个topic，根据messageModel是CLUSTERING、BROADCASTING确定是点对点还是订阅
	 * 不同组：同一个topic，各自消费。新增的组会把之前的消息也消费掉
	 */
	String consumerGroup();
	
	/**
	 * 
	 * 主题
	 */
    String topic();

    /**
     * 广播模式消费： BROADCASTING
     * 集群模式消费： CLUSTERING
     */
    MessageModel messageMode() default MessageModel.CLUSTERING;

    /**
     * 使用线程池并发消费: CONCURRENTLY,
     * 单线程消费: ORDERLY;
     */
    ConsumeMode consumeMode() default ConsumeMode.CONCURRENTLY;
    
    /**
     * 
     * tag  副主题
     */
    String[] tag() default {"*"};
    
    /**
     * 新的consumeGroup从哪里开始消费
     */
    ConsumeFromWhere consumeFromWhere() default ConsumeFromWhere.CONSUME_FROM_LAST_OFFSET;
}
