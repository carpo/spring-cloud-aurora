package org.aurora.mq;

import org.apache.rocketmq.client.log.ClientLogger;
/**
 * springboot 启动类
 * @author created by maven archetype
 *
 */
import org.aurora.mq.annotation.EnableRocketMQConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication(exclude = HibernateJpaAutoConfiguration.class)
@ComponentScan(basePackages = {"org.aurora"})
@EnableRocketMQConfiguration
@RestController
public class SpbootApplication{
	
	public static void main(String[] args) {
		System.setProperty(ClientLogger.CLIENT_LOG_USESLF4J,"true");
		SpringApplication.run(SpbootApplication.class, args);
	}
	
	@Autowired
	ITestBusiness test;
	@RequestMapping("/test")
	public String test() {
		test.test();
		return "test" + Math.random();
	}
}
