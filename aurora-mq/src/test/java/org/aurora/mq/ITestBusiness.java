/**
* Copyright © 2018-2021, by 晓叹星沉.
*/
package org.aurora.mq;

/**
 * 
 * <p>
 * 此处填写类说明
 * </p>
 * @author 晓叹星沉
 * @since jdk1.8
 * 2021年1月8日
 *  
 */

public interface ITestBusiness {

	public void test();
}
