/**
* Copyright © 2018-2019, by 晓叹星沉.
*/
package org.aurora.mq;

import org.apache.rocketmq.common.consumer.ConsumeFromWhere;
import org.apache.rocketmq.common.message.MessageExt;
import org.aurora.mq.annotation.RocketMQConsumer;
import org.aurora.mq.core.AbstractRocketMQPushConsumer;

/**
 * 
 * <p>
 * 此处填写类说明
 * </p>
 * @author 晓叹星沉
 * @since jdk1.8
 * 2019年7月10日
 *  
 */

@RocketMQConsumer(consumerGroup = "consumerGroup3", topic = "test-topic", tag = "tag1", consumeFromWhere = ConsumeFromWhere.CONSUME_FROM_TIMESTAMP)
public class RocketMQConsumerTest1 extends AbstractRocketMQPushConsumer{

	static int num = 0;
	/** 
	  * {@inheritDoc}   
	  * @see org.aurora.mq.core.AbstractRocketMQPushConsumer#process(org.apache.rocketmq.common.message.MessageExt) 
	  */
	@Override
	public boolean process(MessageExt msg) {
		System.out.println("consumerGroup3 consumer : " + new String(msg.getBody()));
		/*if(num == 3){
			return true;
		}
		num++;
		System.out.println("num : " + num + "|返回失败");*/
		return true;
	}

}
