/**
* Copyright © 2018-2019, by 晓叹星沉.
*/
package org.aurora.mq;

import org.apache.rocketmq.common.message.MessageExt;
import org.aurora.mq.annotation.RocketMQConsumer;
import org.aurora.mq.core.AbstractRocketMQPushConsumer;

/**
 * 
 * <p>
 * 此处填写类说明
 * </p>
 * @author 晓叹星沉
 * @since jdk1.8
 * 2019年7月10日
 *  
 */

//@RocketMQConsumer(consumerGroup = "consumerGroup1", topic = "test-topic", tag = "tag1")
public class RocketMQConsumerTest extends AbstractRocketMQPushConsumer{

	/** 
	  * {@inheritDoc}   
	  * @see org.aurora.mq.core.AbstractRocketMQPushConsumer#process(org.apache.rocketmq.common.message.MessageExt) 
	  */
	@Override
	public boolean process(MessageExt msg) {
		System.out.println("consumer : " + new String(msg.getBody()));
		return true;
	}

}
