/**
* Copyright © 2018-2019, by 晓叹星沉.
*/
package org.aurora.mq;

import org.apache.rocketmq.client.exception.MQBrokerException;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.remoting.exception.RemotingException;
import org.aurora.mq.core.DefaultRocketMQProducer;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 
 * <p>
 * 此处填写类说明
 * </p>
 * @author 晓叹星沉
 * @since jdk1.8
 * 2019年7月10日
 *  
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class DefaultRocketMQProducerTest {

	@Autowired
	DefaultRocketMQProducer producer;
	
	//@Test
	public void test() {
		try {
			SendResult ret = producer.send("test-topic", "tag1", "test1", "123".getBytes());
			System.out.println(ret.getSendStatus());
		} catch (MQClientException | RemotingException | MQBrokerException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
