/**
* Copyright © 2017-2018, by 晓叹星沉.
*/
package org.aurora.core.service.impl;

import java.util.List;
import java.util.Map;

import org.aurora.common.model.Page;
import org.aurora.core.service.IBaseService;
import org.aurora.dao.IBaseDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 
 * <p>
 * 基础service服务实现类
 * </p>z
 * @author 晓叹星沉
 * @since jdk1.7
 * 2018年6月2日
 *  
 */
@Primary
@Service
@Transactional
public class BaseService implements IBaseService {

	@Autowired
	private IBaseDAO baseDao;
	/** 
	  * {@inheritDoc}   
	  * @see org.aurora.core.service.IBaseService#get(java.lang.Class, java.lang.String) 
	  */
	@Override
	public <T> T get(Class<T> clazz, String id) {
		return baseDao.get(clazz, id);
	}
	/** 
	  * {@inheritDoc}   
	  * @see org.aurora.core.service.IBaseService#save(java.lang.Object) 
	  */
	@Override
	public boolean save(Object po) {
		baseDao.save(po);
		return true;
	}
	/** 
	  * {@inheritDoc}   
	  * @see org.aurora.core.service.IBaseService#delete(java.lang.Object) 
	  */
	@Override
	public boolean delete(Object po) {
		baseDao.delete(po);
		return true;
	}
	/** 
	  * {@inheritDoc}   
	  * @see org.aurora.core.service.IBaseService#update(java.lang.Object) 
	  */
	@Override
	public boolean update(Object po) {
		baseDao.update(po);
		return true;
	}
	/** 
	  * {@inheritDoc}   
	  * @see org.aurora.core.service.IBaseService#queryListBySql(java.lang.String, java.util.Map, java.lang.Class) 
	  */
	@Override
	public <T> List<T> queryListBySql(String sql, Map<String, ?> params, Class<T> resultObj) {
		return baseDao.queryListBySql(sql, params, resultObj);
	}
	/** 
	  * {@inheritDoc}   
	  * @see org.aurora.core.service.IBaseService#queryListByHql(java.lang.String, java.util.Map) 
	  */
	@Override
	public <T> List<T> queryListByHql(String hql, Map<String, ?> params) {
		return baseDao.queryListByHql(hql, params);
	}
	/** 
	  * {@inheritDoc}   
	  * @see org.aurora.core.service.IBaseService#queryPageByHql(java.lang.String, java.util.Map, int, int) 
	  */
	@Override
	public <T, V> Page<T, V> queryPageByHql(String hql, Map<String, ?> params, int page, int pageSize) {
		return baseDao.queryPageByHql(hql, params, page, pageSize);
	}
	/** 
	  * {@inheritDoc}   
	  * @see org.aurora.core.service.IBaseService#queryPageBySql(java.lang.String, java.util.Map, java.lang.Class, int, int) 
	  */
	@Override
	public <T, V> Page<T, V> queryPageBySql(String sql, Map<String, ?> params, Class<T> resultObj, int page, int pageSize) {
		return baseDao.queryPageBySql(sql, params, resultObj, page, pageSize);
	}
	/** 
	  * {@inheritDoc}   
	  * @see org.aurora.core.service.IBaseService#executeUpdateSql(java.lang.String, java.util.Map) 
	  */
	@Override
	public int executeUpdateSql(String sql, Map<String, ?> params) {
		return baseDao.executeUpdateSql(sql, params);
	}

}
