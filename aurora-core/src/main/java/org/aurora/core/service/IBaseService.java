/**
* Copyright © 2017-2018, by 晓叹星沉.
*/
package org.aurora.core.service;

import java.util.List;
import java.util.Map;

import org.aurora.common.model.Page;

/**
 * 
 * <p>
 * 基础service服务接口
 * </p>
 * @author 晓叹星沉
 * @since jdk1.7
 * 2018年6月2日
 *  
 */

public interface IBaseService {

	/**
     * 保存一条记录
     * @param po 要保存的对象
     * @return 保存成功返回 true， 否则返回 false
     */
    boolean save(Object po);
	/**
     * 根据类型和对象id取得一个对象
     * 
     * @param clazz 类
     * @param id 对象id
     * @param <T> 需要查询的 PO 的类型
     * @return 目标对象
     */
    public <T> T get(Class<T> clazz, String id);
    
    /**
     * 根据主键删除一条记录
     * @param po 要删除的对象
     * @return 删除成功返回 true， 否则返回 false
     */
    boolean delete(Object po);/**
    
    /**
     * 根据主键更新一条记录
     * @param po 要更新的记录，主键值必须不为 null
     * @return 更新成功返回 true， 否则返回 false
     */
    boolean update(Object po);
    
    /**
     * 
     *根据sql查询列表,多表相同字段支持别名映射，但别名应遵循aa_bb（如name as per_name = perName）
     * @param sql  查询sql
     * @param params 参数
     * @param resultObj 结果集对象，用于将查询结果映射到对象中
     * @return
     */
    public <T> List<T> queryListBySql(String sql, Map<String, ?> params, Class<T> resultObj);
    
    /**
     * 
     *根据sql查询列表,多表相同字段支持别名映射，但别名应遵循aa_bb（如name as per_name = perName）
     * @param sql  查询sql
     * @param params 参数
     * @param resultObj 结果集对象，用于将查询结果映射到对象中
     * @param page 当前页
     * @param pageSize 当前页数
     * @return
     */
    public <T, V> Page<T, V> queryPageBySql(String sql, Map<String, ?> params, Class<T> resultObj, int page, int pageSize);
    
    /**
     * 
     * 根据sql查询列表
     * @param clazz
     * @param sql
     * @return
     */
    public <T> List<T> queryListByHql(String hql, Map<String, ?> params);
    
    /**
     * 
     * 根据sql查询分页
     * @param hql
     * @param params 参数
     * @param page 当前页
     * @param pageSize 当前页数
     * @return
     */
    public <T, V> Page<T, V> queryPageByHql(String hql, Map<String, ?> params, int page, int pageSize);
    
    /**
     * 
     * 执行sql
     * @param hql
     * @param params
     * @return
     */
    public int executeUpdateSql(String sql, Map<String, ?> params);
    
}
