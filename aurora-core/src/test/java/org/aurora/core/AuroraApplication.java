package org.aurora.core;

import org.aurora.core.service.IBaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication(exclude = HibernateJpaAutoConfiguration.class)
@ComponentScan(basePackages = {"org.aurora"})
@RestController
public class AuroraApplication {

	public static void main(String[] args) {
		SpringApplication.run(AuroraApplication.class, args);
	}
	
	@Autowired
    private IBaseService bs;
	
	@RequestMapping("/hello")
	public String test() {
		GroupTest g = bs.get(GroupTest.class, "8a81c8a46a9a10a3016a9a10a6a00000");
        System.out.println(g.getName());
		return "1";
	}

}
