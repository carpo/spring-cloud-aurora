/**
* Copyright © 2017-2018, by 晓叹星沉.
*/
package org.aurora.common.constant;

/**
 * 
 * <p>
 * aurora常量类
 * </p>
 * @author 晓叹星沉
 * @since jdk1.8
 * 2019年4月29日
 *  
 */

public class AuroraConstant {

	/** session key*/
	public static final String USER_SESSION = "USER_SESSION";
	
	/** token key*/
	public static final String LOGIN_TOKEN = "token";
	
	/** {@code 401 Unauthorized} (HTTP/1.0 - RFC 1945) */
	public static final String HTTP_STATUS_UNAUTHORIZED = "401";
}
