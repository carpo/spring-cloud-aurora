/**
* Copyright © 2017-2018, by 晓叹星沉.
*/
package org.aurora.common.model;

import java.io.Serializable;

/**
 * 
 * <p>
 * 分页信息对象
 * </p>
 * @author 晓叹星沉
 * @since jdk1.8
 * 2019年4月19日
 *  
 */

public class PageBean implements Serializable {

	/**  */
	private static final long serialVersionUID = 3880415379190714479L;

	/** 每页记录数 */
	private int pageSize = -1;
	
	/** 数据总数 */
	private int total = 0;
	
	/** 当前页数 */
	private int page = 1;

	/**  
	 * PageBean    
	 */
	public PageBean() {
		super();
	}

	/**  
	 * PageBean
	 * @param pageSize
	 * @param page    
	 */
	public PageBean(int page, int pageSize) {
		super();
		this.pageSize = pageSize;
		this.page = page;
	}



	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}
	
	public int getStartNo() {
        return ((getPage() - 1) * getPageSize() + 1);
    }
	
}
