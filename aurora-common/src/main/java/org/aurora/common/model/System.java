/**
* Copyright © 2018-2019, by 晓叹星沉.
*/
package org.aurora.common.model;

import java.io.Serializable;

/**
 * 
 * <p>
 * 系统封装类
 * </p>
 * @author 晓叹星沉
 * @since jdk1.8
 * 2019年5月27日
 *  
 */

public class System implements Serializable {

	/**  */
	private static final long serialVersionUID = 7306931465783125156L;
	/** 系统id*/
	private String systemId;
	/** 系统名称*/
	private String systemName;
	
	public String getSystemId() {
		return systemId;
	}
	public void setSystemId(String systemId) {
		this.systemId = systemId;
	}
	public String getSystemName() {
		return systemName;
	}
	public void setSystemName(String systemName) {
		this.systemName = systemName;
	}
	
	
}
