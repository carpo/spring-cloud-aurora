/**
* Copyright © 2017-2018, by 晓叹星沉.
*/
package org.aurora.common.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**
 * 
 * <p>
 * 角色表
 * </p>
 * @author 晓叹星沉
 * @since jdk1.8
 * 2019年4月30日
 *  
 */
/** 角色表*/
public class Role implements Serializable {

	/**  */
	private static final long serialVersionUID = 8492392775414280579L;
	/** 主键*/
	private String id;
	/** 角色编码*/
	private String roleCode;
	/** 角色名称*/
	private String roleName;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getRoleCode() {
		return roleCode;
	}

	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

}
