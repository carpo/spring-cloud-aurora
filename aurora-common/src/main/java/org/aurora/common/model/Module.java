/**
* Copyright © 2017-2018, by 晓叹星沉.
*/
package org.aurora.common.model;


import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

/**
 * 
 * <p>
 * 模块表
 * </p>
 * @author 晓叹星沉
 * @since jdk1.8
 * 2019年4月23日
 *  
 */
/** 模块表*/
public class Module implements Serializable {

	/**  */
	private static final long serialVersionUID = 8837152909650203910L;
	/** 主键*/
	private String id;
	/** 系统主键*/
	private String sysId;
	/** 模块名称*/
	private String moduleName;
	/** 展示名称*/
	private String dispName;
	/** 模块编码*/
	private String moduleCode;
	/** 是否显示*/
	private Boolean visible;
	/** 模块访问路径*/
	private String moduleUrl;
	/** 附属模块访问路径*/
	private String activeModuleUrl;
	/** 顺序*/
	private double dispOrder = 1F;
	/** 创建时间*/
	private Date createTime;
	/** 父节点id，无父节点则为0*/
	private String parentId;
	
	
	/** 是否叶子节点*/
	private boolean leaf;
	
	/** 子节点*/
	private List<Module> children;
	
	/** 展示名称*/
	private String label;
	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSysId() {
		return sysId;
	}

	public void setSysId(String sysId) {
		this.sysId = sysId;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public String getModuleCode() {
		return moduleCode;
	}

	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}

	public Boolean getVisible() {
		return visible;
	}

	public void setVisible(Boolean visible) {
		this.visible = visible;
	}

	public String getModuleUrl() {
		return moduleUrl;
	}

	public void setModuleUrl(String moduleUrl) {
		this.moduleUrl = moduleUrl;
	}

	public double getDispOrder() {
		return dispOrder;
	}

	public void setDispOrder(double dispOrder) {
		this.dispOrder = dispOrder;
	}
	
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public boolean isLeaf() {
		return leaf;
	}

	public void setLeaf(boolean leaf) {
		this.leaf = leaf;
	}

	public List<Module> getChildren() {
		return children;
	}

	public void setChildren(List<Module> children) {
		this.children = children;
	}

	public String getLabel() {
		return this.dispName;
	}

	public void setLabel(String label) {
		this.label = label;
	}


	public String getDispName() {
		return dispName;
	}

	public void setDispName(String dispName) {
		this.dispName = dispName;
	}

	public String getActiveModuleUrl() {
		return activeModuleUrl;
	}

	public void setActiveModuleUrl(String activeModuleUrl) {
		this.activeModuleUrl = activeModuleUrl;
	}
	
	
	
}
