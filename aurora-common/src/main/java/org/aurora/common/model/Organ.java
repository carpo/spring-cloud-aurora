/**
* Copyright © 2018-2019, by 晓叹星沉.
*/
package org.aurora.common.model;

import java.io.Serializable;

/**
 * 
 * <p>
 * 组织机构po
 * </p>
 * @author 晓叹星沉
 * @since jdk1.8
 * 2019年5月17日
 *  
 */

public class Organ implements Serializable {

	/**  */
	private static final long serialVersionUID = -2678700304130366454L;

	/** 主键 */
    private String organId;

    /** 机构名称 */
    private String organName;

	public String getOrganId() {
		return organId;
	}

	public void setOrganId(String organId) {
		this.organId = organId;
	}

	public String getOrganName() {
		return organName;
	}

	public void setOrganName(String organName) {
		this.organName = organName;
	}
    
}
