/**
* Copyright © 2017-2018, by 晓叹星沉.
*/
package org.aurora.common.model;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * <p>
 * 后台rest返回封装类
 * </p>
 * @author 晓叹星沉
 * @since jdk1.8
 * 2019年4月19日
 *  
 */

public class ResponseObj implements Serializable {

	/**  */
	private static final long serialVersionUID = 1050623143476086430L;

	/** 返回码  0：成功   401:未登录*/
	private int code;
	
	/** 返回信息*/
	private String msg;
	
	/** 返回数据*/
	private Object data;

	
	
	/**  
	 * ResponseObj
	 * @param code
	 * @param msg
	 * @param data    
	 */
	public ResponseObj(int code, String msg, Object data) {
		super();
		this.code = code;
		this.msg = msg;
		this.data = data;
	}

	/**  
	 * ResponseObj    
	 */
	public ResponseObj() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public static ResponseObj retSuccess(){
		ResponseObj res = new ResponseObj(0, "", null);
		return res;
	}
	
	public static ResponseObj retSuccess(Object data){
		ResponseObj res = new ResponseObj(0, "", data);
		return res;
	}
	
	public static ResponseObj retSuccess(String msg, Object data){
		ResponseObj res = new ResponseObj(0, msg, data);
		return res;
	}
	
	public static ResponseObj retFailure(String msg){
		ResponseObj res = new ResponseObj(1, msg, null);
		return res;
	}
	
	public static ResponseObj retFailure(int code, String msg){
		ResponseObj res = new ResponseObj(code, msg, null);
		return res;
	}
	
	public static ResponseObj retFailure(String msg, Object data){
		ResponseObj res = new ResponseObj(1, msg, data);
		return res;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
	
	public static <T, V> Page<T, V> coverList2Page(List<T> list, V params, int currentPage, int pageSize){
		Page<T, V> page = new Page<T, V>();
		page.setParams(params);
		if(null != list){
			if(0 != currentPage && 0 != pageSize){
				int totalPage = (int)Math.ceil((double) list.size() / pageSize); 
				if(currentPage > totalPage){
					currentPage = totalPage;
				}else if(currentPage <= 0){
					currentPage = 1;
				}
				int begin = (currentPage - 1) * pageSize;
				int end = currentPage * pageSize;
				if (end > list.size())
					end = list.size();
				list.subList(begin, end);
			}else{
				page.setResult(list);
				page.getPageBean().setTotal(list.size());
			}
		}else{
			page.getPageBean().setTotal(0);
		}
		return page;
	}
	
}
