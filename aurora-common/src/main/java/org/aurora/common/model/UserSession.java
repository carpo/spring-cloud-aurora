/**
* Copyright © 2017-2018, by 晓叹星沉.
*/
package org.aurora.common.model;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * <p>
 * 用户session信息类
 * </p>
 * @author 晓叹星沉
 * @since jdk1.8
 * 2019年4月29日
 *  
 */

public class UserSession implements Serializable{

	 /**  */
	private static final long serialVersionUID = 5906179639251203211L;

	/** 用户主键 */
    private String userId;
    
    /** 用户名称 */
    private String username;
    
    /** 系统Id */
    private String systemId;
    
    /** 用户身份 */
    private Identity currentIdentity;
    
    /** 用户身份列表 */
    private List<Identity> identities;
    
    /** 系统列表 */
    private List<System> systemList;
    
    /** 模块菜单列表 */
    private List<Module> menuTree;
    
    /** 模块权限列表 */
    private List<Module> moduleAuthority;
    
    /** 按钮权限列表 */
    private List<Operation> operationAuthority;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getSystemId() {
		return systemId;
	}

	public void setSystemId(String systemId) {
		this.systemId = systemId;
	}

	public Identity getCurrentIdentity() {
		return currentIdentity;
	}

	public void setCurrentIdentity(Identity currentIdentity) {
		this.currentIdentity = currentIdentity;
	}

	public List<Identity> getIdentities() {
		return identities;
	}

	public void setIdentities(List<Identity> identities) {
		this.identities = identities;
	}

	public List<System> getSystemList() {
		return systemList;
	}

	public void setSystemList(List<System> systemList) {
		this.systemList = systemList;
	}

	public List<Module> getMenuTree() {
		return menuTree;
	}

	public void setMenuTree(List<Module> menuTree) {
		this.menuTree = menuTree;
	}

	public List<Module> getModuleAuthority() {
		return moduleAuthority;
	}

	public void setModuleAuthority(List<Module> moduleAuthority) {
		this.moduleAuthority = moduleAuthority;
	}

	public List<Operation> getOperationAuthority() {
		return operationAuthority;
	}

	public void setOperationAuthority(List<Operation> operationAuthority) {
		this.operationAuthority = operationAuthority;
	}
    
    
}
