/**
* Copyright © 2018-2019, by 晓叹星沉.
*/
package org.aurora.common.model;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

/**
 * 
 * <p>
 * 用户身份表
 * </p>
 * @author 晓叹星沉
 * @since jdk1.8
 * 2019年5月21日
 *  
 */
/** 用户身份表*/
public class Identity implements Serializable {

	/**  */
	private static final long serialVersionUID = -3131787058607382020L;
	/** 主键*/
	private String id;
	/** 用户id*/
	private String userId;
	/** 角色id*/
	private String roleId;
	/** 角色名称*/
	private String roleName;
	/** 用户组id*/
	private String groupId;
	/** 用户组名称*/
	private String groupName;
	/** 组织id*/
	private String organId;
	/** 组织名称*/
	private String organName;
	/** 创建时间*/
	private Date createTime;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getOrganId() {
		return organId;
	}

	public void setOrganId(String organId) {
		this.organId = organId;
	}

	public String getOrganName() {
		return organName;
	}

	public void setOrganName(String organName) {
		this.organName = organName;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

}
