/**
* Copyright © 2017-2018, by 晓叹星沉.
*/
package org.aurora.common.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * <p>
 * 分页对象
 * </p>
 * @author 晓叹星沉
 * @since jdk1.8
 * 2019年4月19日
 *  
 */
public class Page<T, V> implements Serializable {
	
	/**  */
	private static final long serialVersionUID = 4496409027424272927L;

	private V params;
	
	private PageBean pageBean = new PageBean();
	
	private List<T> result = new ArrayList<T>();

	/**  
	 * Page    
	 */
	public Page() {
		super();
	}
	
	/**  
	 * Page
	 * @param pageBean
	 * @param list    
	 */
	public Page(PageBean pageBean, List<T> result) {
		super();
		this.pageBean = pageBean;
		this.result = result;
	}



	public V getParams() {
		return params;
	}

	public void setParams(V params) {
		this.params = params;
	}

	public PageBean getPageBean() {
		return pageBean;
	}

	public void setPageBean(PageBean pageBean) {
		this.pageBean = pageBean;
	}

	public List<T> getResult() {
		return result;
	}

	public void setResult(List<T> result) {
		this.result = result;
	}

}
