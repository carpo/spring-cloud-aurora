/**
* Copyright © 2017-2018, by 晓叹星沉.
*/
package org.aurora.common.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

/**
 * 
 * <p>
 * 模块按钮
 * </p>
 * @author 晓叹星沉
 * @since jdk1.8
 * 2019年4月25日
 *  
 */
/** 操作按钮*/
public class Operation implements Serializable{

	/**  */
	private static final long serialVersionUID = -7952892109840687706L;
	/** 主键*/
	private String id;
	/** 编码*/
	private String code;
	/** 名称*/
	private String name;
	/** 模块id*/
	private String moduleId;
	/** 创建时间*/
	private Date createTime;
	/** 系统主键*/
	private String systemId;
	
	private boolean isChecked = true;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getModuleId() {
		return moduleId;
	}

	public void setModuleId(String moduleId) {
		this.moduleId = moduleId;
	}
	
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getSystemId() {
		return systemId;
	}

	public void setSystemId(String systemId) {
		this.systemId = systemId;
	}

	public boolean isChecked() {
		return isChecked;
	}

	public void setChecked(boolean isChecked) {
		this.isChecked = isChecked;
	}
	
}
