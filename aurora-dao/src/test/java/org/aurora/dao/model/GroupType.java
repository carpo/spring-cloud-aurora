/**
* Copyright © 2017-2018, by 晓叹星沉.
*/
package org.aurora.dao.model;

/**
 * 
 * 此处填写类简介
 * <p>
 * 此处填写类说明
 * </p>
 * @author 晓叹星沉
 * @since jdk1.8
 * 2019年4月18日
 *  
 */

public enum GroupType {
	AAA("一级"), BBB("二级");
	String desc;
	private GroupType(String desc){
		this.desc = desc;
	}
	

	public String getDesc() {
		return desc;
	}
}