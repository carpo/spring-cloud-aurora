/**
* Copyright © 2017-2018, by 晓叹星沉.
*/
package org.aurora.dao.model;

import java.util.Date;

/**
 * 
 * 此处填写类简介
 * <p>
 * 此处填写类说明
 * </p>
 * @author 晓叹星沉
 * @since jdk1.8
 * 2019年4月18日
 *  
 */

public class PersonGroup {

	private String personName;
	
	private String name;
	
	private String perName;
	
	 private Date creatDate;

	public String getPersonName() {
		return personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getCreatDate() {
		return creatDate;
	}

	public void setCreatDate(Date creatDate) {
		this.creatDate = creatDate;
	}

	public String getPerName() {
		return perName;
	}

	public void setPerName(String perName) {
		this.perName = perName;
	}
	 
}
