package org.aurora.dao;

import org.aurora.dao.model.GroupTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;


@SpringBootTest
public class AuroraApplicationTests {

	@Autowired
    private IBaseDAO baseDao;
	//@Test
	public void contextLoads() {
		GroupTest g = baseDao.get(GroupTest.class, "8a81c8a46a9a10a3016a9a10a6a00000");
        System.out.println(g.getName());
	}

}
