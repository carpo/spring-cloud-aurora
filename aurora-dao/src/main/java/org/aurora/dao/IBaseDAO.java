/**
* Copyright © 2017-2018, by 晓叹星沉.
*/
package org.aurora.dao;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.aurora.common.model.Page;

/**
 * 
 * <p>
 * 数据库访问接口
 * </p>
 * @author 晓叹星沉
 * @since jdk1.8
 * 2018年6月2日
 *  
 */

public interface IBaseDAO {

	/**
     * 保存（新增）一个对象
     * 
     * @param po 要保存的对象
     * @return String 对象主键
     */
    public String save(Object po);
    
    /**
     * 根据类型和对象id从数据库取得一个对象
     * 
     * @param clazz 类
     * @param id 对象id
     * @param <T> 需要查询的 PO 的类型
     * @return 目标对象
     */
    <T> T get(Class<T> clazz, Serializable id);
    
    /**
     * 删除一个对象
     * 
     * @param po 要删除的对象
     */
    void delete(Object po);
    
    /**
     * 更新一个对象
     * 
     * @param po 要修改的对象
     */
    void update(Object po);
    
    /**
     * 
     *根据sql查询列表,多表相同字段支持别名映射，但别名应遵循aa_bb（如name as per_name = perName）
     * @param sql  查询sql
     * @param params 参数
     * @param resultObj 结果集对象，用于将查询结果映射到对象中
     * @return
     */
    <T> List<T> queryListBySql(String sql, Map<String, ?> params, Class<T> resultObj);
    
    /**
     * 
     *根据sql查询列表,多表相同字段支持别名映射，但别名应遵循aa_bb（如name as per_name = perName）
     * @param sql  查询sql
     * @param params 参数
     * @param resultObj 结果集对象，用于将查询结果映射到对象中
     * @param page 当前页
     * @param pageSize 当前页数
     * @return
     */
    <T, V> Page<T, V> queryPageBySql(String sql, Map<String, ?> params, Class<T> resultObj, int page, int pageSize);
    
    /**
     * 
     * 根据sql查询列表
     * @param clazz
     * @param sql
     * @return
     */
    <T> List<T> queryListByHql(String hql, Map<String, ?> params);
    
    /**
     * 
     * 根据sql查询分页
     * @param clazz
     * @param sql
     * @param page 当前页
     * @param pageSize 当前页数
     * @return
     */
    <T, V> Page<T, V> queryPageByHql(String hql, Map<String, ?> params, int page, int pageSize);
    
    /**
     * 
     * 执行sql
     * @param hql
     * @param params
     * @return
     */
    int executeUpdateSql(String sql, Map<String, ?> params);
}
